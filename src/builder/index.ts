/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    NodeBlock,
    Slots,
    conditions,
    each,
    editor,
    pgettext,
    slots,
    tripetto,
} from "tripetto";
import { URLCondition } from "./condition";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_DEFINED from "../../assets/icon-defined.svg";
import ICON_UNDEFINED from "../../assets/icon-undefined.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:url", "URL");
    },
})
export class URL extends NodeBlock {
    urlSlot!: Slots.String;

    @slots
    defineSlot(): void {
        this.urlSlot = this.slots.static({
            type: Slots.String,
            reference: "url",
            label: URL.label,
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.options();
        this.editor.required(this.urlSlot);
        this.editor.visibility();
        this.editor.alias(this.urlSlot);
        this.editor.exportable(this.urlSlot);
    }

    @conditions
    defineCondition(): void {
        each(
            [
                {
                    mode: "defined" as "defined",
                    label: pgettext("block:url", "URL is specified"),
                    icon: ICON_DEFINED,
                },
                {
                    mode: "undefined" as "undefined",
                    label: pgettext("block:url", "URL is not specified"),
                    icon: ICON_UNDEFINED,
                },
            ],
            (condition) => {
                this.conditions.template({
                    condition: URLCondition,
                    label: condition.label,
                    icon: condition.icon,
                    props: {
                        slot: this.urlSlot,
                        mode: condition.mode,
                    },
                });
            }
        );
    }
}
