/** Package information defined using webpack */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    ConditionBlock,
    Forms,
    affects,
    definition,
    editor,
    pgettext,
    tripetto,
} from "tripetto";
import { URL } from "./";
import { TConditionMode } from "../runner/mode";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_DEFINED from "../../assets/icon-defined.svg";
import ICON_UNDEFINED from "../../assets/icon-undefined.svg";

@tripetto({
    type: "condition",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    context: URL,
    icon: ICON,
    get label() {
        return pgettext("block:url", "Verify URL");
    },
})
export class URLCondition extends ConditionBlock {
    @definition
    @affects("#name")
    mode: TConditionMode = "defined";

    get icon() {
        return this.mode === "defined" ? ICON_DEFINED : ICON_UNDEFINED;
    }

    // Return an empty label, since the node name is in the block name already.
    get label() {
        return "";
    }

    get name() {
        if (this.slot) {
            switch (this.mode) {
                case "defined":
                    return `@${this.slot.id} ${pgettext(
                        "block:url",
                        "specified"
                    )}`;
                case "undefined":
                    return `@${this.slot.id} ${pgettext(
                        "block:url",
                        "not specified"
                    )}`;
            }
        }

        return this.type.label;
    }

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:url", "When URL:"),
            controls: [
                new Forms.Radiobutton<TConditionMode>(
                    [
                        {
                            label: pgettext("block:url", "Is specified"),
                            value: "defined",
                        },
                        {
                            label: pgettext("block:url", "Is not specified"),
                            value: "undefined",
                        },
                    ],
                    Forms.Radiobutton.bind(this, "mode", "defined")
                ),
            ],
        });
    }
}
